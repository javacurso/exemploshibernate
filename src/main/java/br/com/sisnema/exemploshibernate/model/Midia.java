package br.com.sisnema.exemploshibernate.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Midia {

	@Id
	private Integer codigo;
	private Boolean inutilizada;

	@ManyToOne
	@JoinColumn(name="cod_filme")
	private Filme filme;
	
	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Filme getFilme() {
		return filme;
	}

	public void setFilme(Filme filme) {
		this.filme = filme;
	}

	public Boolean getInutilizada() {
		return inutilizada;
	}

	public void setInutilizada(Boolean inutilizada) {
		this.inutilizada = inutilizada;
	}

}
