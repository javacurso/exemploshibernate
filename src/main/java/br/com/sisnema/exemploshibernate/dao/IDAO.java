package br.com.sisnema.exemploshibernate.dao;

import java.util.List;

public interface IDAO<T> {

	void salvar(T pojo);

	void excluir(T pojo);

	T obterPorId(T filtro);

	List<T> pesquisar(T filtros);
}
