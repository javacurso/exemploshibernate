package br.com.sisnema.exemploshibernate.dao;

import org.hibernate.Session;

import br.com.sisnema.exemploshibernate.util.JPAUtil;

public abstract class DAO<T> implements IDAO<T> {
	
	private Session sessionPostgres;
	private Session sessionMysql;

	public DAO() {
		sessionPostgres = JPAUtil.getEntityManagerPostgres().unwrap(Session.class);
		sessionMysql = JPAUtil.getEntityManagerMysql().unwrap(Session.class);
	}

	protected final Session getSessionPostgres() {
		return sessionPostgres;
	}

	protected final Session getSessionMysql() {
		return sessionMysql;
	}
}
