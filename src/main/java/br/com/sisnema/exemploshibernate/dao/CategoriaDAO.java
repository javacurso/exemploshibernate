package br.com.sisnema.exemploshibernate.dao;

import java.util.List;

import org.hibernate.Session;

import br.com.sisnema.exemploshibernate.model.Categoria;

public class CategoriaDAO extends DAO<Categoria> {

	private Session session;

	public void salvar(Categoria pojo) {
		session.beginTransaction();
		session.saveOrUpdate(pojo);
		session.getTransaction().commit();
	}

	public void excluir(Categoria pojo) {
		session.beginTransaction();
		session.delete(pojo);
		session.getTransaction().commit();
	}

	public Categoria obterPorId(Categoria filtro) {
		return session.get(Categoria.class, filtro.getCodigo());
	}

	public List<Categoria> pesquisar(Categoria filtros) {
		return session.createCriteria(Categoria.class).list();
	}

	protected final Session getSession() {
		return session;
	}

	protected final void setSession(Session session) {
		this.session = session;
	}

}
