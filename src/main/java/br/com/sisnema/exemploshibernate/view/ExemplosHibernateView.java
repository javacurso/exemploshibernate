package br.com.sisnema.exemploshibernate.view;

import static javax.swing.JOptionPane.showInputDialog;

import java.util.Objects;

import br.com.sisnema.exemploshibernate.dao.CategoriaDAOMysql;
import br.com.sisnema.exemploshibernate.dao.CategoriaDAOPostgres;
import br.com.sisnema.exemploshibernate.model.Categoria;

public class ExemplosHibernateView {

	public static void main(String[] args) {
		criarCategorias();
		listarCategorias();
	}

	private static void listarCategorias() {
		new CategoriaDAOMysql().pesquisar(new Categoria())
							   .forEach(c -> System.out.println(c));
		
		new CategoriaDAOPostgres().pesquisar(new Categoria())
							      .forEach(c -> System.out.println(c));
	}

	private static void criarCategorias() {
		do {
			
			String descricao = showInputDialog("Informe o nome de uma categoria (-1 sair)");
			
			if(Objects.equals("-1", descricao)) {
				break;
			}
			
			new CategoriaDAOMysql().salvar(new Categoria(descricao));
			new CategoriaDAOPostgres().salvar(new Categoria(descricao));
			
		} while(true);
	}
}
